package main

import (
	"fmt"
	"os"
	"pla/internal/commands"
	"pla/internal/shell"
)

func main() {
	fmt.Println("Running pla...")

	currentShell, err := shell.Get()

	if err != nil {
		fmt.Printf("Error: %s", err)

		os.Exit(1)
	}

	history, err := shell.GetHistory(currentShell)

	if err != nil {
		fmt.Printf("Error: %s", err)

		os.Exit(1)
	}

	command, oldCommands := commands.Build(history)
	similar := commands.GetSimilar(command, oldCommands)

	fmt.Printf("Command is: [%s]\n", command)
	fmt.Printf("Similar commands are: [%v]", similar)
}
