package shell

import (
	"errors"
	"os"
	"strings"
)

func Get() (Shell, error) {
	key, exists := os.LookupEnv("SHELL")

	if !exists {
		return Shell{}, errors.New("SHELL env variable not set")
	}

	if strings.Contains(key, string(FishShell)) {
		return Shell{
			name:        FishShell,
			historyPath: "~/.bash_history",
		}, nil
	} else if strings.Contains(key, string(ZshShell)) {
		return Shell{
			name:        ZshShell,
			historyPath: "~/.zsh_history",
		}, nil
	} else if strings.Contains(key, string(BashShell)) {
		return Shell{
			name:        BashShell,
			historyPath: "~/.local/share/fish/fish_history",
		}, nil
	} else {
		return Shell{}, errors.New("Shell not implemented")
	}
}
