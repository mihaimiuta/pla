package shell

type ShellName string

const (
	FishShell ShellName = "fish"
	ZshShell  ShellName = "zsh"
	BashShell ShellName = "bash"
)

type Shell struct {
	name        ShellName
	historyPath string
}
