package shell

import (
	"os"
	"path/filepath"
	"strings"
)

func readHistoryFile(path string) (string, error) {
	if strings.HasPrefix(path, "~/") {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return "", err
		}
		path = filepath.Join(homeDir, path[2:])
	}

	absolutePath, err := filepath.Abs(path)

	if err != nil {
		return "", err
	}

	fileBytes, err := os.ReadFile(absolutePath)

	if err != nil {
		return "", err
	}

	return string(fileBytes), nil
}

func GetHistory(shell Shell) (string, error) {
	history, err := readHistoryFile(shell.historyPath)

	if err != nil {
		return "", err
	}

	return history, nil
}
