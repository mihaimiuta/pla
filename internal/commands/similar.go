package commands

import "github.com/samber/lo"

func getSameLengthCommands(command string, oldCommands []string) []string {
	return lo.Filter(oldCommands, func(item string, _ int) bool {
		return len(item) == len(command)
	})
}

func countMatchingCharacters(command string, oldCommand string) int {
	count := 0
	oldCommandRunes := []rune(oldCommand)

	for index, commandRune := range []rune(command) {
		if commandRune == oldCommandRunes[index] {
			count += 1
		}
	}

	return count
}

func GetSimilar(command string, oldCommands []string) []string {
	sameLength := getSameLengthCommands(command, oldCommands)
	currentMatchingCharacterCount := 0
	var similar []string

	for _, oldCommand := range sameLength {
		matchingCharacterCount := countMatchingCharacters(command, oldCommand)

		if matchingCharacterCount == currentMatchingCharacterCount {
			similar = append(similar, oldCommand)
		} else {
			currentMatchingCharacterCount = matchingCharacterCount
			similar = []string{oldCommand}
		}
	}

	return similar
}
