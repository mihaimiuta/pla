package commands

import (
	"strings"

	"github.com/samber/lo"
)

func getHistoryArray(history string) []string {
	lines := strings.Split(history, "\n")

	return lo.Filter(lines, func(item string, _ int) bool {
		return item != ""
	})
}

func getOldCommands(historyArray []string) []string {
	raw := historyArray[0 : len(historyArray)-2]

	return lo.Reverse(raw)
}

func Build(history string) (string, []string) {
	historyArray := getHistoryArray(history)
	command := historyArray[len(historyArray)-1]

	return command, getOldCommands(historyArray)
}
